$(document).ready(function() {

  //accion del boton del formulario --- Para registro
  $("#botonenviar").click( function() { 

      //recibir y asignar a variables lo que llega del form
      cedula = $('#cedula').val();
      nombre = $('#nombre').val();
      apellidos = $('#apellidos').val();
      telef = $('#telef').val();
      correo = $('#correo').val();
      direccion = $('#direccion').val();
      observ = $('#observ').val();

      //agg lo anterior a un array
      dataEmpleados = {
        cedula : cedula,
        nombre  : nombre,
        apellidos : apellidos,
        telef : telef,
        correo : correo,
        direccion : direccion,
        observ : observ,
        
      }
    
      dataEmpleados = JSON.stringify(dataEmpleados); //El método JSON.stringify() convierte un objeto o valor de JavaScript en una cadena de texto JSON

      //console.log(dataEmpleados); //imprimir la anterior cadena en consola ... 
    

      $.ajax({
        url: 'http://localhost/miproyecto/Empleados/registrar',
        data: {action: 'createEmpleado', data: dataEmpleados},
        dataType: 'HTML',
        type: 'POST',
        success: function(resp) 
        {       
           alert("Registro Satisfactorio!");

           //Limpiar las cajas de texto -- solo las tipo text
           $(":text").each(function(){	
            $($(this)).val('');
        });

        //limpia todo el form
         $("#form")[0].reset();

    
        }, // FIN DEL success
        
        error: function(xhr, ajaxOptions, thrownError) 
        { 
             
    
        }  
    });
    

  })



 //accion del boton del formulario --- Para Update
 $("#botonupdate").click( function() { 

  //recibir y asignar a variables lo que llega del form
  cedula = $('#cedula').val();
  nombre = $('#nombre').val();
  apellidos = $('#apellidos').val();
  telef = $('#telef').val();
  correo = $('#correo').val();
  direccion = $('#direccion').val();
  observ = $('#observ').val();

  //agg lo anterior a un array
  dataEmpleados = {
    cedula : cedula,
    nombre  : nombre,
    apellidos : apellidos,
    telef : telef,
    correo : correo,
    direccion : direccion,
    observ : observ,
    
  }

  dataEmpleados = JSON.stringify(dataEmpleados); //El método JSON.stringify() convierte un objeto o valor de JavaScript en una cadena de texto JSON

  //console.log(dataEmpleados); //imprimir la anterior cadena en consola ... 


  $.ajax({
    url: 'http://localhost/miproyecto/Empleados/registrar',
    data: {action: 'UpdateEmpleado', data: dataEmpleados},
    dataType: 'HTML',
    type: 'POST',
    success: function(resp) 
    {       
       alert("Datos actualizados con éxito!");

       //Limpiar las cajas de texto -- solo las tipo text
       $(":text").each(function(){	
        $($(this)).val('');
    });

    //limpia todo el form
     $("#form")[0].reset();


    }, // FIN DEL success
    
    error: function(xhr, ajaxOptions, thrownError) 
    { 
         

    }  
});


})


  

}); // fin del ready function

function actualizar()
  {
    $("#botonUpdate").click( function() { 

      alert('update');

      location.reload();
      })
  }