<?php

class Empleados_model extends CI_Model
{

	/*
	*
	*
	*
	*/

	public function __construct()
	{

		parent::__construct();

		$this->lang->load('employes');

	}

	public function getAll()
    {
   
        $this->db->select('*');
		$this->db->from('mp_empleados');
		$consulta = $this->db->get();
		$Usuario = $consulta->result_array();
		
		return $Usuario;


	}
	

	public function getAllTwo($id)
    {
   
        $this->db->select('*');
		$this->db->from('mp_empleados');
		$this->db->where('tbl_emple_cedula="'.$id.'"');
        $consulta = $this->db->get();
		$Usuario = $consulta->result_array();

		return $Usuario;


    }


	public function createEmpleado($data_employe)
	{
		$data_employe = json_decode($data_employe); //Decodifica el string que le llega del controlador

		//se asigana a variables lo que se recibe
		$cedula = $data_employe->cedula;
		$nombre  = strtoupper($data_employe->nombre);
		$apellidos = strtoupper($data_employe->apellidos);
		$telef = $data_employe->telef;
		$correo = strtoupper($data_employe->correo);
		$direccion = strtoupper($data_employe->direccion);
		$observ = strtoupper($data_employe->observ);
		
		
		$this->db->trans_start(); //inicia la transacción .... 
		$sql = "INSERT INTO mp_empleados (tbl_emple_cedula, tbl_emple_nombres, tbl_emple_apellidos, tbl_emple_telef, tbl_emple_correo, tbl_emple_direccion, tbl_emple_observ) VALUES
		('".$cedula."', '".$nombre."', '".$apellidos."', '".$telef."', '".$correo."', '".$direccion."', '".$observ."')";
		$query = $this->db->query($sql);
		$ep_id = $this->db->insert_id(); //ultimo id insertado
		$this->db->trans_complete();

		


		if($this->db->trans_status() === TRUE)
		{
		
			$response = array(
			'respon_code' => 1,
			'ep_id' => $ep_id,
			'respon_msg' => 'Se registro con éxito la información . . .',
		);

	} else {

		$response = array(
			'respon_code' => 0,
			//'respon_msg' => lang('not_insert_employe') // Esto me da error con la function lang()
		);

	}

		return json_encode($response);


	} //Fin de la function createEmpleado



	public function updateEmpleado($data_employe)
	{

		$data_employe = json_decode($data_employe); //Decodifica el string que le llega del controlador

		//se asigana a variables lo que se recibe
		$cedula = $data_employe->cedula;
		$nombre  = strtoupper($data_employe->nombre);
		$apellidos = strtoupper($data_employe->apellidos);
		$telef = $data_employe->telef;
		$correo = strtoupper($data_employe->correo);
		$direccion = strtoupper($data_employe->direccion);
		$observ = strtoupper($data_employe->observ);

		
		
		$this->db->trans_start(); //inicia la transacción .... 
		$sql = "UPDATE mp_empleados SET tbl_emple_nombres = '".$nombre."', tbl_emple_apellidos = '".$apellidos."', tbl_emple_telef = '".$telef."', tbl_emple_correo = '".$correo."',
		 tbl_emple_direccion = '".$direccion."', tbl_emple_observ = '".$observ."' WHERE tbl_emple_cedula = '".$cedula."'";
		$query = $this->db->query($sql);
		$this->db->trans_complete();

		


		if($this->db->trans_status() === TRUE)
		{
		
			$response = array(
			'respon_code' => 1,
			'ep_id' => $ep_id,
			'respon_msg' => 'Datos actualizados con éxito . . .',
		);

	} else {

		$response = array(
			'respon_code' => 0,
			//'respon_msg' => lang('not_insert_employe') // Esto me da error con la function lang()
		);

	}

		return json_encode($response);


	} //Fin de la function createEmpleado




}