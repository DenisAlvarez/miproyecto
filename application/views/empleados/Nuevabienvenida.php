<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    

    <title>Bienvenidos!</title>

  </head>


  <body>
  <!-- Contenedor que da la bienvenida .... -->

  <div class="alert alert-success" role="alert">
  <h4 class="alert-heading">Hola, bienvenidos!</h4>
  <br>
  <p> es un pequeño sistema para los empleados con información muy útil  y su objetivo principal es facilitar el ingreso y consultas de los mismos en la base de datos .... </p>
  <hr>
  <p class="mb-0">Puedes agregar, mas detalles . . .</p>
  </div>
  <br>

    
  <!-- Buttones  modal -->
<button type="button" class="btn btn-outline-warning" data-toggle="modal" data-target="#exampleModalScrollable">
  Registro
</button>

<button  type="button" id="botonUpdate"  class="btn btn-outline-dark" data-toggle="modal" data-target="#ventanaConsulta">Consulta</button>

<!-- <a href="<?= base_url('usuarios');?>" >Url Válida</a>  -->

<!-- Modal para las consultas -->
<div class="modal fade" id="ventanaConsulta" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
<div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalCenterTitle">Consulta de Empleados</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          
      <table class="table table-dark table-striped">
    <thead>
      <tr>
      <tr>
        <th>Cédula</th>
        <th>Nombres</th>
        <th>Apellidos</th>
        <th>Teléfono</th>
        <th>Correo</th>
        <th>Dirección</th>
        <th>Observaciones</th>
      </tr>
    </thead>

    <?php $url = base_url('update'); ?>

    <tbody>
      <tr>
        
        <td>
          <?php  foreach($Usuarios as $row) 
          { 
            echo "<tr>"; //Columnas
                echo '<td><a href="'.$url.'/'.$row["tbl_emple_cedula"].'">'.$row['tbl_emple_cedula']. '</a></td>';
               // echo "<td>" .$row['tbl_emple_cedula']. "</td>"; //Filas
                echo "<td>" .$row['tbl_emple_nombres']. "</td>"; //Filas
                echo "<td>" .$row['tbl_emple_apellidos']. "</td>"; //Filas
                echo "<td>" .$row['tbl_emple_telef']. "</td>"; //Filas
                echo "<td>" .$row['tbl_emple_correo']. "</td>"; //Filas
                echo "<td>" .$row['tbl_emple_direccion']. "</td>"; //Filas
                echo "<td>" .$row['tbl_emple_observ']. "</td>"; //Filas
            echo "<tr>";
          } 
?></td>
      </tr>
    </tbody>
  </table>
</div>
      </div>
      
    </div>
  </div>
</div>


<!-- Ventana Modal de Registo-->
<div class="modal fade" id="exampleModalScrollable" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalScrollableTitle">Formulario de Registro</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <form id="form" name="form" method="POST">

              <div class="form-group">
                  <label for="cedula">Cédula <span>(requerido)</span></label>
                  <input type="text" name="cedula" id="cedula" class="form-control" placeholder="17333555">
              </div>


              <div class="form-group">
                  <label for="nombre">Nombre (s) <span>(requerido)</label>
                  <input type="text" name="nombre" id="nombre" class="form-control" placeholder="Juan David">
              </div>

              <div class="form-group">
                  <label for="apellidos">Apellido (s) <span>(requerido)</span></label>
                  <input type="text" name="apellidos" id="apellidos" class="form-control" placeholder="Rojas Rojas">
              </div>


              <div class="form-group">
              <label for="telef">Teléfono <span>(requerido)</span></label>
                  <input type="text" name="telef" id="telef" class="form-control" placeholder="0212-8607412">
              </div>


              <div class="form-group">
              <label for="correo">Correo <span>(requerido)</span></label>
                  <input type="text" name="correo" id="correo" class="form-control" placeholder="algo@gmail.com">
                  <small id="emailHelp" class="form-text text-muted">Su correo no sera publicado.</small>
              </div>


              <div class="form-group">
                  <label for="direccion">Dirección <span>(requerido)</span></label>
                  <textarea class="form-control" name="direccion"  id="direccion" rows="3"></textarea>
                  </div>


                  <div class="form-group">
                  <label for="observ">Observaciones <span>(requerido)</span></label>
                  <textarea class="form-control" name="direccion" id="observ" rows="3"></textarea>
                  </div>

        </form>

      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <input type="button" id="botonenviar" value="Registrarme!!" class="btn btn-outline-primary">
        
    </div>
  </div>
</div>


</body>

<!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>


    <!--agg los js,  librerias jquery !-->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="http://localhost/miproyecto/js/archivo.js" ></script>


</html>