<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Formulario de Registro</title>


<!--Forma de incluir, pero ... da error base_url !-->
<!--<link href="<?php //base_url('estilos/style.css')?>" rel="stylesheet" type="text/css">!-->
<!-- <script type="text/javascript" src="<?php //echo base_url();?>js/archivo.js" ></script>!-->

<!--agg la hoja de estilos !-->
<!-- <link href="http://localhost/miproyecto/estilos/style.css" rel="stylesheet" type="text/css"> !-->

<link href="http://localhost/miproyecto/css/bootstrap.min.css" rel="stylesheet" type="text/css"> 


<!--agg los js !-->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript" src="http://localhost/miproyecto/js/archivo.js" ></script>

</head>


<body>

<div class="container-fluid bg-primary">

    <h2><b> Actualizar datos del empleado: <?= $Usuarios[0]['tbl_emple_nombres']?></b></h2>

</div>


<div class="alert alert-danger" role="alert">
  Es importante completar todos los datos, a modificar ...!
</div>


</br> 


<form id="form" name="form" method="POST">

<div class="form-group">
    <label for="cedula">Cédula <span>(requerido)</span></label>
    <input type="text" name="cedula" id="cedula" class="form-control"  readonly="on"  value="<?= $Usuarios[0]['tbl_emple_cedula']?>">
    <input type="hidden" id="cedula" name="cedula" value="<?=$Usuarios[0]['tbl_emple_cedula']?>">
</div>


<div class="form-group">
    <label for="nombre">Nombre (s) <span>(requerido)</label>
    <input type="text" name="nombre" id="nombre" class="form-control" value="<?= $Usuarios[0]['tbl_emple_nombres']?>">
</div>


<div class="form-group">
    <label for="apellidos">Apellido (s) <span>(requerido)</span></label>
    <input type="text" name="apellidos" id="apellidos" class="form-control" value="<?= $Usuarios[0]['tbl_emple_apellidos']?>">
</div>


<div class="form-group">
<label for="telef">Teléfono <span>(requerido)</span></label>
    <input type="text" name="telef" id="telef" class="form-control" value="<?= $Usuarios[0]['tbl_emple_telef']?>">
</div>


<div class="form-group">
<label for="correo">Correo <span>(requerido)</span></label>
    <input type="text" name="correo" id="correo" class="form-control" value="<?= $Usuarios[0]['tbl_emple_correo']?>">
    <small id="emailHelp" class="form-text text-muted">Su correo no sera publicado.</small>
</div>


<div class="form-group">
<label for="direccion">Dirección <span>(requerido)</span></label>
<textarea class="form-control" name="direccion"  id="direccion" rows="3"></textarea>
</div>


<div class="form-group">
<label for="observ">Observaciones <span>(requerido)</span></label>
<textarea class="form-control" name="direccion" id="observ" rows="3"></textarea>
</div>


</br> </br> 

<input type="button" id="botonupdate" value="Actualizar!!" class="btn btn-outline-primary">

<a href="<?=base_url('bienvenida')?>">Inicio</a> 

</form>


</body>
</html>