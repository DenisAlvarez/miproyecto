<?php

class Usuarios extends CI_Controller
{

    public function saludo()
    {
        
        $this->load->model('Usuarios_model', 'saludar'); //leer el modelo, nombreModel, Alias

        $datos['Usuarios'] = $this->saludar->getAll(); //ejecutar lo anterior       

        $this->load->view('usuarioSaludo.php', $datos); //lee la vista, y se envia como parametros $datos

    }
    
}