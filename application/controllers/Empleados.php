<?php

class Empleados extends CI_Controller
{


/* 

  **** Este bloque de codigo, genera error:  con la function lang y con array_push ****

  public function __construct()
	{
		parent:: __construct();

    $this->load->model('Empleados_model', 'employes');
		$this->lang->load('general');
    $this->lang->load('employes');

    array_push(
			$this->filesAssest['css_files'],
			'estilos/style.css'
		);

		array_push(
			$this->filesAssest['js_files'],
			'js/employes'
		);

	}
*/

    public function formularioEmpleados()
    {
        
        //cargar la vista ....
        $this->load->view('empleados/formulario');
        
    }


    public function updateEmpleados($id)
    {
     
      

      $this->load->model('Empleados_model', 'empleados'); //leer el modelo, nombreModel, Alias

      $datos['Usuarios'] = $this->empleados->getAllTwo($id); //ejecutar lo anterior 
      
         
      //cargar la vista ....
      $this->load->view('empleados/formularioUpdate',$datos); //lee la vista, y se envia como parametros $Usuarios
        
        
        
    }

    public function Bienvenida()
    {

      $this->load->model('Empleados_model', 'empleados'); //leer el modelo, nombreModel, Alias

      $datos['Usuarios'] = $this->empleados->getAll(); //ejecutar lo anterior 

         
      //cargar la vista ....
      $this->load->view('empleados/Nuevabienvenida',$datos); //lee la vista, y se envia como parametros $Usuarios
        
    }

  


    public function registrar()
    {
      
      $action = $this->input->post_get('action'); //Recibe la accion a ejecutar, desde el js
      $data_employe = $this->input->post_get('data'); //Recibe la data a procesar
    

      if($action == 'createEmpleado')
      {

        $this->load->model('Empleados_model', 'empleados'); //Leer el modelo -- agg un alias
        
        $employe_respon = $this->empleados->createEmpleado($data_employe); // Va al modelo empleados y ejecuta la funcion createEmpleado con la información enviada como parametro $data_employe
        
        $this->output->set_content_type('application/json')->set_output($employe_respon); //salida

      }
      else if($action == 'UpdateEmpleado')
      {

        $this->load->model('Empleados_model', 'empleados'); //Leer el modelo -- agg un alias

        $employe_respon = $this->empleados->updateEmpleado($data_employe); // Va al modelo empleados y ejecuta la funcion createEmpleado con la información enviada como parametro $data_employe
        
        $this->output->set_content_type('application/json')->set_output($employe_respon); //salida

      }
    }


    private function loadTemplate()
	{
		if($this->session->userdata('us_session') === TRUE){
			$this->assets->initialize($this->filesAssest);
			$this->load->view('master_template', $this->data);
		} else {
			redirect(base_url());
		}

	}

}