<?php defined('BASEPATH') OR exit('No direct script access allowed');


/*
* Lenguaje Login
*/

$lang['title_page'] = 'Inicio de sesion';
$lang['form_user'] ='Nombre de usuario o dirección de correo.';
$lang['form_password'] ='Contraseña';
$lang['send_form'] = 'Acceder';
$lang['user_error'] = 'Datos de usuario o contraseña invalida.';
$lang['user_disabled'] = 'Usuario inactivo, contacte al administrador.';
$lang['user_enabled'] = 'Acceso aprobado, espere un momento.';
$lang['user_eraccess'] = 'Error accediendo al sistema, contacte al administrador';

// code 0 usuario erroneo o no existente
// code 1 usuario activo y correcto
// code 2 usuario inactivo
// code 3 usuario por aprobar inactivo
// code 4 error actualizando token

/*
* Lenguaje Modulo Inicio
*/

$lang['title_mod_home'] = 'Inicio del sistema';

/*
* Lenguaje Modulo empleados
*/

$lang['title_mod_employes'] = 'Empleados';

/*
* Lenguaje Modulo usuarios
*/

$lang['title_mod_user'] = 'Usuarios';

/*
* Lenguaje Modulo auditoria del sistema
*/

$lang['action_access'] = 'Inicio de sesion en el sistema';
