<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
* Lenguaje Modulo Usuarios
*/

$lang['create_user'] = 'Crear Usuario';
$lang['update_user'] = 'Editar Usuario';
$lang['name_user'] = 'Usuario';

$lang['title_create_user'] = 'Crear Usuario';
$lang['title_update_user'] = 'Editar Usuario';
$lang['success_insert_user'] = 'Usuario registrado exitosamente.';
$lang['success_update_user'] = 'Usuario Actualizado exitosamente.';
$lang['not_insert_user'] = 'Ha ocurrido un problema en el sistema intente mas tarde.';
$lang['not_update_user'] = 'Ha ocurrido un problema en el sistema intente mas tarde.';
$lang['status_us'] = 'Estado del usuario';
$lang['success_insert_user'] = 'Usuario registrado exitosamente.';
$lang['success_update_user'] = 'Usuario actualizado exitosamente.';
$lang['return_list_user'] = 'Volver al listado de usuarios';
