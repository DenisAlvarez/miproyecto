<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
* Lenguaje Modulo empleados
*/

$lang['create_employe'] = 'Crear Empleado';
$lang['update_employe'] = 'Editar Empleado';
$lang['name_employe'] = 'Nombre';
$lang['departament_employe'] = 'Departamento';
$lang['position_employe'] = 'Cargo';
$lang['title_create_employe'] = 'Crear Empleado';
$lang['title_update_employe'] = 'Editar Empleado';
$lang['verify-employe-ic'] = 'Compruebe si el empleado ya ha sido registrado ingresando el numero de cedula';
$lang['identity_legal_employe'] = 'Nº de identificación legal del empleado.';
$lang['identity_fiscal_employe'] = 'Nº de identificación fiscal del empleado.';
$lang['num_identifycation_employe'] = 'Nº que identifica al empleado en la empresa';
$lang['date_entry'] = 'Fecha de ingreso del empleado';
$lang['date_egress'] = 'Fecha de egreso del empleado';
$lang['success_insert_employe'] = 'Empleado registrado exitosamente.';
$lang['success_update_employe'] = 'Empleado Actualizado exitosamente.';
$lang['not_insert_employe'] = 'Ha ocurrido un problema en el sistema intente mas tarde.';
$lang['not_update_employe'] = 'Ha ocurrido un problema en el sistema intente mas tarde.';
$lang['status'] = 'Estado del empleado';
