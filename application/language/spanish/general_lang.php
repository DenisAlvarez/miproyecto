<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
* Lenguaje Modulo Inicio
*/

$lang['title_mod_home'] = 'Inicio del sistema';

/*
* Lenguaje Modulo empleados
*/

$lang['title_mod_employes'] = 'Empleados';

/*
* Lenguaje Modulo usuarios
*/

$lang['title_mod_users'] = 'Usuarios';

/*
* Lenguaje Modulo roles y permisos
*/

$lang['title_mod_rol'] = 'Roles y Permisos';

/*
* Lenguaje modulo modulos
*/

$lang['title_mod_module'] = 'Modulos';

/*
* Lenguaje modulo departamentos
*/

$lang['title_mod_departament'] = 'Departamentos';


/*
* Lenguaje modulo Cargos
*/

$lang['title_mod_position'] = 'Cargos';


/*
* Lenguaje Modulo auditoria del sistema
*/

$lang['action_access'] = 'Inicio de sesion en el sistema';
$lang['action_insert_employe'] = 'Registro de empleado en el sistema';
$lang['action_update_employe'] = 'Edicion de empleado en el sistema';
$lang['action_insert_user'] = 'Registro de usuario en el sistema';
$lang['action_update_user'] = 'Edicion de usuario en el sistema';
$lang['action_insert_mod'] = 'Registro de modulo en el sistema';
$lang['action_update_mod'] = 'Edicion de modulo en el sistema';
$lang['action_insert_permissin'] = 'Registro de permisos en el sistema';
$lang['action_update_permissin'] = 'Edicion de permisos en el sistema';
$lang['action_insert_dp'] = 'Registro de departamentos en el sistema';
$lang['action_update_dp'] = 'Edicion de departamentos en el sistema';
$lang['action_insert_ps'] = 'Registro de cargos en el sistema';
$lang['action_update_ps'] = 'Edicion de cargos en el sistema';

/*
* Lenguaje de uso general
*/

$lang['action_tables'] = 'Acciones';
$lang['saved_action'] = 'Guardar';
$lang['return_action'] = 'Volver';
$lang['ok_exists_ep'] = 'El Nº de cedula ingresado ya se encuentra registrado.';
$lang['not_exists_ep'] = 'El Nº de cedula ingresado no se encuentra registrado.';
$lang['search'] = 'Buscar';
$lang['identity_card'] = 'Nº de identificación legal';
$lang['identity_fiscal'] = 'Nº de identificación fiscal';
$lang['first_name'] = 'Primer nombre';
$lang['last_name'] = 'Segundo nombre';
$lang['lastfirst_name'] = 'Primer apellido';
$lang['lastlast_name'] = 'Segundo apellido';
$lang['code_employe'] = 'Código de empleado';
$lang['datebirth'] = 'Fecha de nacimiento';
$lang['phone_mobile'] = 'Telefono movil';
$lang['phone_fixed'] = 'Telefono Fijo';
$lang['email'] = 'Correo electronico';
$lang['direction'] = 'Direccion';
$lang['return_list_employe'] = 'Volver al listado de empleados';
$lang['user_name'] = 'Nombre de usuario';
$lang['email_corporate'] = 'Correo Corporativo';
$lang['name'] = 'Nombre';
$lang['description'] = 'Descripcion';
$lang['position'] = 'Cargo';
$lang['departament'] = 'Departamento';
$lang['status'] = 'Estado';
$lang['searchname'] = 'Búsqueda por nombre';
$lang['searchdepartament'] = 'Búsqueda por departamento';
$lang['searchposition'] = 'Búsqueda por cargo';
$lang['searchemail'] = 'Búsqueda por correo';
$lang['search_employe_id'] = 'Busqueda del empleado por numero de identificacion legal.';
$lang['exists_user_employe'] = 'Para este numero de identificacion legal ya existe un usuario registrado.';
$lang['exists_employe_not'] = 'El numero de identificacion legal no se encuentra registrado en el sistema.';
$lang['dates_employe'] = 'Datos del empleado al cual desea crearle un usuario';
$lang['us_permissin'] = 'Permisos de usuario';
$lang['email_exists'] = 'El correo ingresado ya se encuentra registrado en el sistema';
