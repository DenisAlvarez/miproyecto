<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
* Lenguaje Modulo Cargos
*/

$lang['create_position'] = 'Crear Cargo';
$lang['ps_dp'] = 'Departamento al que pertenece';
$lang['ps_name'] = 'Nombre del cargo';
$lang['ps_description'] = 'Descripcion del cargo';
$lang['return_list_ps'] = 'Volver al listado de cargos';
$lang['success_insert_ps'] = 'Registro de cargo exitoso';
$lang['success_update_ps'] = 'Actualizacion de cargo exitoso';
$lang['title_create_position'] = 'Registro de cargo';
$lang['title_update_position'] = 'Editar cargo';
