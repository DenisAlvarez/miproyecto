<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
* Lenguaje Modulo departamentos
*/

$lang['create_departament'] = 'Crear Departamento';
$lang['dp_parent'] = 'Departamento superior';
$lang['dp_name'] = 'Nombre del departamento';
$lang['dp_description'] = 'Descripcion del departamento';
$lang['return_list_dp'] = 'Volver al listado de departamentos';
$lang['success_insert_dp'] = 'Registro de departamento exitoso';
$lang['success_update_dp'] = 'Actualizacion de departamento exitoso';
$lang['title_create_departament'] = 'Registro de departamento';
$lang['title_update_departament'] = 'Editar departamento';
