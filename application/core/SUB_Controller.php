<?php defined('BASEPATH') OR exit('No direct script access allowed');



class SUB_Controller extends CI_Controller
{

	protected $filesAssest = [];
	protected $data;

    public function __construct()
    {
            parent::__construct();

            $this->filesAssest['css_files'] = [
            	'css/bootstrap',
							'css/all',
							'DataTables/datatables',
							'DataPicker/css/gijgo',
            	'css/style',
							'css/custom-style'
            ];

						$this->filesAssest['js_files'] = [
          	'js/jquery-3.4.1',
						'js/popper',
						'js/bootstrap',
						'DataTables/datatables',
						'DataPicker/js/gijgo',
						'DataPicker/js/messages/messages.es-es',
						'js/jquery.validate',
						'js/script',
						'js/custom-script'
        ];

            $this->data = new stdClass();
    }
}
