<?php

class Assets
{

  protected $CI;
  protected $css_path = '';
  protected $js_path = '';
  protected $img_path = '';
  protected $version = '';
  protected $ext = '';
  protected $css_files = [];
  protected $js_files = [];
  protected $img_files = '';

	public function __construct($params = [])
	{

        if (count($params) > 0) {
            $this->initialize($params);
        }


	}

	public function initialize($params = [])
	{

		foreach ($params as $key => $value) {
            if (isset($this->$key)) {
              $this->$key = $value;
            }
        }

	}


	public function load_css()
	{

		$content = null;

        foreach ($this->css_files as $pos => $file_name){

            $this->ext = '.css';
            $file = $file_name . $this->ext;
            $file_path = path_asset($this->css_path . $file);

            if (file_exists($file_path)){

                $version = '?v' . date('Ymd-B', fileatime($file_path));
                $this->ext = (ENVIRONMENT === 'testing' || ENVIRONMENT === 'production') ? '.min.css' : '.css';

            } else {

                $version = '';
                $this->ext = '.min.css';

            }


            $file_url = $this->css_path . $file_name . $this->ext . $version;
            $content .= '<link  rel="stylesheet" href="' . base_asset($file_url) . '"/>'."\n";


        }

        return $content;

	}





	public function load_js()
	{

				$content = null;

        foreach ($this->js_files as $pos => $file_name) {
            $this->ext = '.js';
            $file = $file_name . $this->ext;
            $file_path = path_asset($this->js_path . $file);

            if (file_exists($file_path)) {
                $version = '?v' . date('Ymd-B', fileatime($file_path));
                $this->ext = (ENVIRONMENT === 'testing' || ENVIRONMENT === 'production') ? '.min.js' : '.js';
            } else {
                $version = '';
                $this->ext = '.min.js';
            }

            $file_url = $this->js_path . $file_name . $this->ext . $version;

            $content .= '<script src="' . base_asset($file_url) . '"></script>'."\n";
        }

        return $content;

	}

}
