$(function(){

  /*
  * Registro de departamentos
  */
$('#saved_form_departament').on('click', function(e){
    e.preventDefault();

    dp_parent = $('#dp_parent').val();
    dp_name = $('#dp_name').val();
    dp_description = $('#dp_description').val();

    data_departament = {
      dp_parent : dp_parent,
      dp_name : dp_name,
      dp_description : dp_description
    };

    data_departament = JSON.stringify(data_departament);

    createDepartament(data_departament);

  });

  /*
  * Edicion de departamentos
  */
$('#saved_update_form_departament').on('click', function(e){
    e.preventDefault();

    dp_id = $('#dp_id').val();
    dp_parent = $('#dp_parent').val();
    dp_name = $('#dp_name').val();
    dp_description = $('#dp_description').val();

    data_departament = {
      dp_id : dp_id,
      dp_parent : dp_parent,
      dp_name : dp_name,
      dp_description : dp_description
    };

    data_departament = JSON.stringify(data_departament);

    updateDepartament(data_departament);

  });

});

/**************************************************************************************************************************************************************************************************/

/*
* Funcion para crear los departamentos
*/

function createDepartament(data_departament){

   $.ajax({

      method: 'POST',
      url: 'registrar-departamento',
      data: {action: 'createDepartament', data: data_departament},
      beforeSend: function(){

        $('#saved_form_departament').prop('disabled', true);
        $('#saved_form_departament').html('<span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>&nbsp;Procesando ...');

      }
    }).done(function(response){

      code = response.respon_code;
      msg = response.respon_msg;

      if(code == 1){
        window.setTimeout( function(){ window.location = '/editar-departamento/'+response.dp_id+'/2/1'; }, 1000);
      }

    }).fail(function(response){


  });

}


/*
* Funcion para editar los departamentos
*/

function updateDepartament(data_departament){

   $.ajax({

      method: 'POST',
      url: '/edicion-departamento',
      data: {action: 'updateDepartament', data: data_departament},
      beforeSend: function(){

        $('#saved_update_form_departament').prop('disabled', true);
        $('#saved_update_form_departament').html('<span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>&nbsp;Procesando ...');

      }
    }).done(function(response){

      code = response.respon_code;
      msg = response.respon_msg;

      if(code == 1){
        window.setTimeout( function(){ window.location = '/editar-departamento/'+response.dp_id+'/1/2'; }, 1000);
      }

    }).fail(function(response){


  });

}
