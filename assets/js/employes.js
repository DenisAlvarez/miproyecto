/**
*
*
*/

$(function(){

  /*
  * Listado de empleados
  */

  var dataTable = $('#list-employes').DataTable({
    drawCallback: function(d){
      $('[data-toggle="tooltip"]').tooltip();
		},'language': {
			'url': 'assets/js/datatable-espanol.json'
		},
    autoWidth: false,
		destroy: true,
    lengthChange: true,
		ordering: true,
		pagingType: 'first_last_numbers',
		pagelength: 10,
		processing: true,
		searching: true,
		serverSide: true,
		ajax: {
			url: '/lista-empleados',
			data: function(d){
        var
        searchname = $('#searchname').val();
        searchdepartament = $('#searchdepartament').val();
        searchposition = $('#searchposition').val();

        d.searchname = searchname;
        d.searchdepartament = searchdepartament;
        d.searchposition = searchposition;

			}
    },
    columns: [
			{data: function(data){
        full_name = data.ep_firstname +' '+data.ep_firstlastname;
        return full_name;
      }},
			{data: 'ep_departament'},
			{data: 'ep_codeposition'},
			{data: function(data){
        return '<a href="/editar-empleado/'+data.ep_id+'/1/3" class="btn btn-info btn-sm" data-toggle="tooltip" data-placement="top" title="Modificar">'+
                '<i class="fas fa-pen"></i>'+
              '</a>';
      }}
    ],

  });

  /*
  * Funcion que busca empleados por nombre
  */

  $('#searchname').on('keyup', function(){
    dataTable.draw();
  });

  /*
  * Funcion que busca empleados por departamento
  */

  $('#searchdepartament').on('change', function(){
    dataTable.draw();
  });

  /*
  * Funcion que busca empleados por cargo
  */

  $('#searchposition').on('change', function(){
    dataTable.draw();
  });

  /*
  * Verifica si el empleado ya ha sido registrado anteriormente
  */

  $('#send-employe-identity-card').on('click', function(e){

    e.preventDefault();

    identity_card = $('#search_employe_identity_card').val();

    if(identity_card != ''){

        $('#search_employe_identity_card').removeClass('error');

        dataEp = {
          identity_card : identity_card
        }

        identity_card = JSON.stringify(dataEp);

        verifyEmploye(identity_card);

    } else {
        $('#search_employe_identity_card').addClass('error');
    }


  });

  /*
  * Combo box departamento - cargo
  */

  $('#departament').on('change', function(){
    dp_id = $(this).val();
    if(dp_id != 0){
      $('#position option[ps="'+dp_id+'"]').show();
      $('#position').prop('disabled', false);
    } else {
      $('#position option.position').hide();
      $('#position').prop('disabled', true);
      $('#position option[ps="0"]').prop('selected', true);
    }

  });

  /*
  * Datepicker fecha de ingreso a la empresa
  */

  var date = new Date();

  $('#dateentry').datepicker({
     uiLibrary: 'bootstrap4',
     locale: 'es-es',
     format: 'dd-mm-yyyy',
     minDate : function(){
            var date = new Date();
            date.setDate(date.getDate()+1);
            return new Date(date.getFullYear() - 90, date.getMonth(), date.getDate());
      },
     maxDate: function(){
            var date = new Date();
            date.setDate(date.getDate()+1);
            return new Date(date.getFullYear(), date.getMonth(), date.getDate());
      }
  });

  /*
  * Datepicker fecha de nacimiento del empleado
  */

  $('#datebirth').datepicker({
     uiLibrary: 'bootstrap4',
     locale: 'es-es',
     format: 'dd-mm-yyyy',
     minDate : function(){
            var date = new Date();
            date.setDate(date.getDate()+1);
            return new Date(date.getFullYear() - 90, date.getMonth(), date.getDate());
      },
     maxDate: function(){
            var date = new Date();
            date.setDate(date.getDate()+1);
            return new Date(date.getFullYear() - 18, date.getMonth(), date.getDate());
      }
  });

  /*
  * Datepicker fecha de egreso del empleado
  */

  $('#dateegress').datepicker({
    uiLibrary: 'bootstrap4',
    locale: 'es-es',
    format: 'dd-mm-yyyy',
    minDate : function(){
           var date = new Date();
           date.setDate(date.getDate()+1);
           return new Date(date.getFullYear() - 90, date.getMonth(), date.getDate());
     },
    maxDate: function(){
           var date = new Date();
           date.setDate(date.getDate()+1);
           return new Date(date.getFullYear(), date.getMonth(), date.getDate());
     }
  });

  /*
  * Evento que envia la informacion a la funcion crear empleado
  */

  $('#saved_form_employe').on('click', function(e){

    e.preventDefault();

    identity_card = $('#identity_card').val();
    type_fiscal = $('#type_fiscal').val();
    identity_fiscal = $('#identity_fiscal').val();
    code_fiscal = $('#code_fiscal').val();
    firstname = $('#firstname').val();
    lastname = $('#lastname').val();
    lastfirstname = $('#lastfirstname').val();
    lastlastname = $('#lastlastname').val();
    datebirth = $('#datebirth').val();
    departament = $('#departament').val();
    position = $('#position').val();
    dateentry = $('#dateentry').val();
    phone_mobile = $('#phone_mobile').val();
    phone_fixed = $('#phone_fixed').val();
    email = $('#email').val();
    direction = $('#direction').val();

    dataEmploye = {
      identity_card : identity_card,
      type_fiscal  : type_fiscal,
      identity_fiscal : identity_fiscal,
      code_fiscal : code_fiscal,
      firstname : firstname,
      lastname : lastname,
      lastfirstname : lastfirstname,
      lastlastname : lastlastname,
      datebirth : datebirth,
      departament : departament,
      position : position,
      dateentry : dateentry,
      phone_mobile : phone_mobile,
      phone_fixed : phone_fixed,
      email : email,
      direction : direction
    }

    dataEmploye = JSON.stringify(dataEmploye);

    validate_employes();

    if(jQuery('#form_employe').valid()){

    createEmploye(dataEmploye);

    }

  });


  $('#saved_update_form_employe').on('click', function(e){

    e.preventDefault();

    ep_id = $('#ep_id').val();
    type_fiscal = $('#type_fiscal').val();
    identity_fiscal = $('#identity_fiscal').val();
    code_fiscal = $('#code_fiscal').val();
    firstname = $('#firstname').val();
    lastname = $('#lastname').val();
    lastfirstname = $('#lastfirstname').val();
    lastlastname = $('#lastlastname').val();
    datebirth = $('#datebirth').val();
    departament = $('#departament').val();
    position = $('#position').val();
    dateentry = $('#dateentry').val();
    dateegress = $('#dateegress').val();
    status = $('#status').val();
    phone_mobile = $('#phone_mobile').val();
    phone_fixed = $('#phone_fixed').val();
    email = $('#email').val();
    direction = $('#direction').val();

    dataEmploye = {
      ep_id : ep_id,
      type_fiscal  : type_fiscal,
      identity_fiscal : identity_fiscal,
      code_fiscal : code_fiscal,
      firstname : firstname,
      lastname : lastname,
      lastfirstname : lastfirstname,
      lastlastname : lastlastname,
      datebirth : datebirth,
      departament : departament,
      position : position,
      dateentry : dateentry,
      dateegress : dateegress,
      status : status,
      phone_mobile : phone_mobile,
      phone_fixed : phone_fixed,
      email : email,
      direction : direction
    }

    dataEmploye = JSON.stringify(dataEmploye);

    validate_employes();

    if(jQuery('#form_employe').valid()){

      updateEmploye(dataEmploye);

    }


  });

});

/*
* Verificamos via ajax si el usuario ya ha sido registrado en la base de datos
*/

function verifyEmploye(identity_card){
  $.ajax({

    method: 'POST',
    url: 'verificar-empleado',
    data: {action: 'verifyEp', data: identity_card},
    beforeSend: function(){

      $('#send-employe-identity-card').empty().prop('disabled', true);
      $('#send-employe-identity-card').html('<span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>&nbsp;Procesando ...');

    }
  }).done(function(response){

      code = response.respon_code;
      msg = response.respon_msg;

      if(code == 1){

        ep_id = response.ep_id;
        fullname = response.fullname;
        departament = response.departament;
        position = response.position;

        $('#notifyModalCenter').modal('show');
        $('#notifyModalCenterTitle').html('<i class="fas fa-exclamation-triangle"></i>&nbsp Notificación');
        $('#msg-modal').text(response.respon_msg);

        html = '<ul class="list-group list-group-flush">'+
                '<li class="list-group-item">Nombre: '+fullname+'</li>'+
                '<li class="list-group-item">Departamento: '+departament+'</li>'+
                '<li class="list-group-item">Cargo: '+position+'</li>'+
                '<li class="list-group-item"><a href="'+baseUrl+'/editar-empleado/'+ep_id+'/1">Editar empleado</a></li>'+
              '</ul>'

        $('#content-modal-notify').html(html);

        $('#send-employe-identity-card').empty().prop('disabled', false);
        $('#send-employe-identity-card').html('Buscar');

        $('#search_employe_identity_card').val(' ');

      } else {

        $('#notifyModalCenter').modal('show');
        $('#notifyModalCenterTitle').html('<i class="fas fa-exclamation-triangle"></i>&nbsp Notificación');
        $('#msg-modal').text(response.respon_msg);

        $('.confirm-action').on('click', function(){
          identity_card = $('#search_employe_identity_card').val();
          $('#content-verify-employe').remove();
          $('#content-form-employe').show();

          $('#saved_form_employe').prop('disabled', false);
          $('#identity_card').val(identity_card);
          $('#identity_fiscal').val(identity_card);

        });

      }

  }).fail(function(response){


});

}

/*
* Funcion que crea al empleado
*/

function createEmploye(dataEmploye){

  $.ajax({

    method: 'POST',
    url: 'registrar-empleado',
    data: {action: 'createEmploye', data: dataEmploye},
    beforeSend: function(){

      $('#saved_form_employe').empty().prop('disabled', true);
      $('#saved_form_employe').html('<span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>&nbsp;Procesando ...');
      $('#msg-modal').empty();

    }
  }).done(function(response){

    if(response.respon_code == 1){

      window.setTimeout( function(){ window.location = '/editar-empleado/'+response.ep_id+'/2/1'; }, 1000);

    } else {

      $('#notifyModalCenter').modal('show');
      $('#notifyModalCenterTitle').html('<i class="fas fa-exclamation-triangle"></i>&nbsp Notificación');
      $('#msg-modal').text(response.respon_msg);

    }

  }).fail(function(response){


});
}

/*
* Funcion que edita al empleado
*/

function updateEmploye(dataEmploye){
  console.log(dataEmploye);

  $.ajax({

    method: 'POST',
    url: '/edicion-empleado',
    data: {action: 'updateEmploye', data: dataEmploye},
    beforeSend: function(){

      $('#saved_update_form_employe').empty().prop('disabled', true);
      $('#saved_update_form_employe').html('<span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>&nbsp;Procesando ...');
      $('#msg-modal').empty();

    }
  }).done(function(response){

    if(response.respon_code == 1){

      window.setTimeout( function(){ window.location = '/editar-empleado/'+response.ep_id+'/1/2'; }, 1000);

    } else {

      $('#notifyModalCenter').modal('show');
      $('#notifyModalCenterTitle').html('<i class="fas fa-exclamation-triangle"></i>&nbsp Notificación');
      $('#msg-modal').text(response.respon_msg);

    }

  }).fail(function(response){


});

}
