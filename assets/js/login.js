$(function(){

/*
* Evento que envia los datos a la funcion userAccess
*/

$('#send-login').on('click', function(e){

  e.preventDefault();

  user = $('#user').val();
  password = $('#password').val();

  dataUser = {
    user : user,
    password : password
  };

  dataUser = JSON.stringify(dataUser);

  if(user == ''){

    $('#user').val('').addClass('error');
    $('#password').removeClass('error');
    $('#notifyModalCenter').modal('show');
    $('#notifyModalCenterTitle').html('<i class="fas fa-exclamation-triangle"></i>&nbsp Notificación');
    $('#msg-modal').text('Ingrese un nombre de usuario o correo.');

  } else if(password == ''){

    $('#user').removeClass('error');
    $('#password').val('').addClass('error');
    $('#notifyModalCenter').modal('show');
    $('#notifyModalCenterTitle').html('<i class="fas fa-exclamation-triangle"></i>&nbsp Notificación');
    $('#msg-modal').text('Ingrese una contraseña para el usuario.');

  } else if(user == '' && password == ''){

    $('#user').val('').addClass('error');
    $('#password').val('').addClass('error');
    $('#notifyModalCenter').modal('show');
    $('#notifyModalCenterTitle').html('<i class="fas fa-exclamation-triangle"></i>&nbsp Notificación');
    $('#msg-modal').text('Debe completar los datos en el formulario.');

  } else {

    $('#user').removeClass('error');
    $('#password').removeClass('error');
    userAccess(dataUser);

  }


});

});


/*
* Funcion que envia los datos de acceso del usuario
*/

function userAccess(dataUser){

  $.ajax({

    method: 'POST',
    url: 'acceder',
    data: {action: 'accessUser', data: dataUser},
    beforeSend: function(){

      $('#send-login').empty().prop('disabled', true);
      $('#send-login').html('<span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>&nbsp;Procesando ...');

      $('#user').removeClass('error');
      $('#password').removeClass('error');

    }
  }).done(function(response){

    code = response.respon_code;
    msg = response.respon_msg;

    if(code == 1){

      window.location = 'inicio';

    } else {

      $('#notifyModalCenter').modal('show');
      $('#notifyModalCenterTitle').html('<i class="fas fa-exclamation-triangle"></i>&nbsp Notificación');
      $('#msg-modal').text(response.respon_msg);

      $('#send-login').empty().prop('disabled', false);
      $('#send-login').text('Acceder');

      $('#user').val('').addClass('error');
      $('#password').val('').addClass('error');

    }

  }).fail(function(response){

      $('#send-login').empty().prop('disabled', false);
      $('#send-login').text('Acceder');

      $('#notifyModalCenter').modal('show');
      $('#notifyModalCenterTitle').html('<i class="fas fa-exclamation-triangle"></i>&nbsp Notificación');
      $('#msg-modal').text(response.respon_msg);

});

}
