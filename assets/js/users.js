/**
*
*
*/

$(function(){

  /*
  * Listado de empleados
  */

  var dataTable = $('#list-users').DataTable({
    drawCallback: function(d){
      $('[data-toggle="tooltip"]').tooltip();
		},'language': {
			'url': 'assets/js/datatable-espanol.json'
		},
    autoWidth: false,
		destroy: true,
    lengthChange: true,
		ordering: true,
		pagingType: 'first_last_numbers',
		pagelength: 10,
		processing: true,
		searching: true,
		serverSide: true,
		ajax: {
			url: '/listado-usuarios',
			data: function(d){
        var
        searchname = $('#searchname').val();
        searchdepartament = $('#searchdepartament').val();
        searchposition = $('#searchposition').val();
        searchemail = $('#searchemail').val();

        d.searchname = searchname;
        d.searchdepartament = searchdepartament;
        d.searchposition = searchposition;
        d.searchemail = searchemail;

			}
    },
    columns: [
			{data: 'us_name'},
			{data: 'us_departament'},
			{data: 'us_position'},
      {data: 'us_username'},
      {data: 'us_email'},
      {data: function(data){
          if(data.us_status == 1){
            return 'Activo';
          } else {
            return 'Inactivo';
          }
      }},
			{data: function(data){
        return '<a href="/editar-usuario/'+data.us_id+'/1/3" class="btn btn-info btn-sm" data-toggle="tooltip" data-placement="top" title="Modificar">'+
                '<i class="fas fa-pen"></i>'+
              '</a>';
      }}
    ],

  });

  /*
  * Funcion que busca usuarios por nombre
  */

  $('#searchname').on('keyup', function(){
    dataTable.draw();
  });

  /*
  * Funcion que busca usuarios por departamento
  */

  $('#searchdepartament').on('change', function(){
    dataTable.draw();
  });

  /*
  * Funcion que busca de usuario por cargo
  */

  $('#searchposition').on('change', function(){
    dataTable.draw();
  });

  /*
  * Funcion que busca usuarios por correo
  */

  $('#searchemail').on('keyup', function(){
    dataTable.draw();
  });

  /*
  * Verifica si el empleado ya ha sido registrado anteriormente
  */

  $('#send-employe-identity-card').on('click', function(e){

    e.preventDefault();

    identity_card = $('#search_employe_identity_card').val();

    if(identity_card != ''){

        $('#search_employe_identity_card').removeClass('error');

        dataEp = {
          identity_card : identity_card
        }

        identity_card = JSON.stringify(dataEp);

        searchEmploye(identity_card);

    } else {
        $('#search_employe_identity_card').addClass('error');
    }


  });


  /*
  * Funcion que toma los datos para el registro del usuario
  */

  $('#saved_form_user').on('click', function(){

    employe_id = $('#employe_id').val();
    us_name = $('#us_name').val();
    us_email = $('#us_email').val();
    us_permissin = $('#us_permissin').val();
    us_status = $('#us_status').val();

    data_user = {
      ep_id : employe_id,
      us_name : us_name,
      us_email : us_email,
      us_permissin : us_permissin,
      us_status : us_status
    };

    data_user = JSON.stringify(data_user);

    createUser(data_user);

  });

  /*
  * Funcion que toma los datos para la edicion del usuario
  */

  $('#saved_update_form_user').on('click', function(){

    us_id = $('#us_id').val();
    us_email = $('#us_email').val();
    us_permissin = $('#us_permissin').val();
    us_status = $('#us_status').val();

    data_user = {
      us_id : us_id,
      us_email : us_email,
      us_permissin : us_permissin,
      us_status : us_status,
    };

    data_user = JSON.stringify(data_user);

    updateUser(data_user);

  });

});





/*
* Funcion que busca al empleado para ser asignado al usuario
*/

/*
* Verificamos via ajax si el usuario ya ha sido registrado en la base de datos
*/

function searchEmploye(identity_card){
  $.ajax({

    method: 'POST',
    url: 'buscar-empleado',
    data: {action: 'verifyEp', data: identity_card},
    beforeSend: function(){

      $('#send-employe-identity-card').empty().prop('disabled', true);
      $('#send-employe-identity-card').html('<span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>&nbsp;Procesando ...');

    }
  }).done(function(response){

      code = response.respon_code;
      msg = response.respon_msg;

      if(code == 1){

        ep_id = response.ep_id;
        fullname = response.fullname;
        departament = response.departament;
        position = response.position;

        $('#notifyModalCenter').modal('show');
        $('#notifyModalCenterTitle').html('<i class="fas fa-exclamation-triangle"></i>&nbsp Notificación');
        $('#msg-modal').text(response.respon_msg);

        html = '<ul class="list-group list-group-flush">'+
                '<li class="list-group-item">Nombre: '+fullname+'</li>'+
                '<li class="list-group-item">Departamento: '+departament+'</li>'+
                '<li class="list-group-item">Cargo: '+position+'</li>'+
              '</ul>';

        $('#content-modal-notify').html(html);

        $('#send-employe-identity-card').empty().prop('disabled', false);
        $('#send-employe-identity-card').html('Buscar');

        $('#search_employe_identity_card').val('');



        $('.confirm-action').on('click', function(){

          $('#content-modal-notify').empty();
          $('#msg-modal').empty();

          $('#identity_card_user').val(response.identity_card);
          $('#name_employe_user').val(fullname);
          $('#departament_employe_user').val(departament);
          $('#position_employe_user').val(position);
          $('#employe_id').val(ep_id);

          $('#us_name').val(generateNameUser(fullname, response.identity_card));

          $('#saved_form_user').prop('disabled', false);

        });



      } else {

        $('#notifyModalCenter').modal('show');
        $('#notifyModalCenterTitle').html('<i class="fas fa-exclamation-triangle"></i>&nbsp Notificación');
        $('#msg-modal').text(response.respon_msg);

        $('.confirm-action').on('click', function(){

          $('#send-employe-identity-card').empty().prop('disabled', false);
          $('#send-employe-identity-card').html('Buscar');

          $('#identity_card_user').val('');
          $('#name_employe_user').val('');
          $('#departament_employe_user').val('');
          $('#position_employe_user').val('');
          $('#employe_id').val('');

          $('#us_name').val('');
          $('#us_email').val('');

          $('#saved_form_user').prop('disabled', true);

          $('#search_employe_identity_card').val('');

        });

      }

  }).fail(function(response){


});

}

/*
* Funcion que crea al usuario
*/

function createUser(data_user){

  $.ajax({

    method: 'POST',
    url: 'registrar-usuario',
    data: {action: 'createUser', data: data_user},
    beforeSend: function(){

      $('#saved_form_user').empty().prop('disabled', true);
      $('#saved_form_user').html('<span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>&nbsp;Procesando ...');

    }
  }).done(function(response){

      code = response.respon_code;
      msg = response.respon_msg;

      if(code == 1){
        window.setTimeout( function(){ window.location = '/editar-usuario/'+response.us_id+'/2/1'; }, 1000);
      } else {
        $('#notifyModalCenter').modal('show');
        $('#notifyModalCenterTitle').html('<i class="fas fa-exclamation-triangle"></i>&nbsp Notificación');
        $('#msg-modal').text(msg);
      }

  }).fail(function(response){


});


}

/*
* Funcion que edita al usuario
*/

function updateUser(data_user){

    $.ajax({

      method: 'POST',
      url: '/edicion-usuario',
      data: {action: 'updateUser', data: data_user},
      beforeSend: function(){

        $('#saved_update_form_user').empty().prop('disabled', true);
        $('#saved_update_form_user').html('<span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>&nbsp;Procesando ...');

      }
    }).done(function(response){

        code = response.respon_code;
        msg = response.respon_msg;

        if(code == 1){
          window.setTimeout( function(){ window.location = '/editar-usuario/'+response.us_id+'/1/2'; }, 1000);
        } else {
          $('#notifyModalCenter').modal('show');
          $('#notifyModalCenterTitle').html('<i class="fas fa-exclamation-triangle"></i>&nbsp Notificación');
          $('#msg-modal').text(msg);
          $('#saved_update_form_user').empty().prop('disabled', false);
          $('#saved_update_form_user').html('Guardar');
        }

    }).fail(function(response){


  });

}

/*
* Funcion que genera el nombre de usuario
*/

function generateNameUser(fullname, identity_card){

    user_name = fullname.substring(0, fullname.indexOf(' ')).toLowerCase();

    return user_name+identity_card;

}
