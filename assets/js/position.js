$(function(){

  /*
  * Registrar cargo en el sistema
  */

  $('#saved_form_position').on('click', function(){

    ps_dp = $('#ps_dp').val();
    ps_name = $('#ps_name').val();
    ps_description = $('#ps_description').val();

    data_ps = {
      ps_dp : ps_dp,
      ps_name : ps_name,
      ps_description : ps_description
    };

    data_ps = JSON.stringify(data_ps);

    createPosition(data_ps);

  });

});

/******************************************************************************************************************************/


function createPosition(data_ps){

    $.ajax({

      method: 'POST',
      url: 'registrar-cargo',
      data: {action: 'createPosition', data: data_ps},
      beforeSend: function(){

        $('#saved_form_position').prop('disabled', true);
        $('#saved_form_position').html('<span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>&nbsp;Procesando ...');

      }
    }).done(function(response){

      code = response.respon_code;
      msg = response.respon_msg;

      if(code == 1){
        window.setTimeout( function(){ window.location = '/editar-cargo/'+response.ps_id+'/2/1'; }, 1000);
      }

    }).fail(function(response){


  });

}
