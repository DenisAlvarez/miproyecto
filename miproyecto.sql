-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 26-12-2019 a las 13:47:56
-- Versión del servidor: 10.3.16-MariaDB
-- Versión de PHP: 7.3.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `miproyecto`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mp_empleados`
--

CREATE TABLE `mp_empleados` (
  `tbl_emple_id` int(11) NOT NULL,
  `tbl_emple_cedula` int(10) NOT NULL,
  `tbl_emple_nombres` varchar(100) NOT NULL,
  `tbl_emple_apellidos` varchar(100) NOT NULL,
  `tbl_emple_telef` int(15) NOT NULL,
  `tbl_emple_correo` varchar(300) NOT NULL,
  `tbl_emple_direccion` varchar(300) NOT NULL,
  `tbl_emple_observ` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `mp_empleados`
--

INSERT INTO `mp_empleados` (`tbl_emple_id`, `tbl_emple_cedula`, `tbl_emple_nombres`, `tbl_emple_apellidos`, `tbl_emple_telef`, `tbl_emple_correo`, `tbl_emple_direccion`, `tbl_emple_observ`) VALUES
(1, 17347300, 'Denis D', 'Alvarez C', 2147483647, 'ddac87@gmail.com', 'Manicomio', 'Ninguna'),
(2, 7664150, 'David Alejandro', 'Lopez Guerra', 2124449874, 'Nada@gmail.com', 'Manicomio', 'Ninguna'),
(3, 4921950, 'Leida ', 'Jara', 2147483647, 'vdfjfjv@gmail.com', 'Los Frailes', 'Ninguna'),
(4, 14333888, 'Carlos Albaerto', 'Lugo Lugo', 2147483647, 'gggggg@gmail.com', 'Nada ', 'Ninguna'),
(5, 7664188, 'Darielys Alejandra', 'Alvarez Alvarez', 2147483647, 'algo@gmail.com', 'Ninguna', 'Nada por ahora...');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_usuarios`
--

CREATE TABLE `tbl_usuarios` (
  `id` int(11) NOT NULL,
  `nombres_usuario` varchar(100) NOT NULL,
  `telef_usuarios` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbl_usuarios`
--

INSERT INTO `tbl_usuarios` (`id`, `nombres_usuario`, `telef_usuarios`) VALUES
(1, 'DENIS ALVAREZ', '4242576163');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `mp_empleados`
--
ALTER TABLE `mp_empleados`
  ADD PRIMARY KEY (`tbl_emple_id`);

--
-- Indices de la tabla `tbl_usuarios`
--
ALTER TABLE `tbl_usuarios`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `mp_empleados`
--
ALTER TABLE `mp_empleados`
  MODIFY `tbl_emple_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `tbl_usuarios`
--
ALTER TABLE `tbl_usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
